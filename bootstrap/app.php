<?php

class Torch
{
    protected static $instance = null;

    public $container;


    private function __construct()
    {
        $this->autoload();
        $this->bootstrap();
    }

    protected function autoload()
    {
        // Check if there is a autoload.php file.
        // Meaning we're in development mode or
        // the plugin has been installed on a "classic" WordPress configuration.
        if (file_exists($autoload = __DIR__ . '/../vendor/autoload.php')) {
            require $autoload;
        }
        if (WP_ENV !== 'production' && !is_admin()) {
            $whoops = new \Whoops\Run();
            $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler());
            $whoops->register();
        }
    }

    protected function bootstrap()
    {
        $this->container = new \App\Foundation\Application();

        // Create a request from server variables, and bind it to the container; optional
        $request = \Illuminate\Http\Request::capture();
        $this->container->instance('request', $request);

        Illuminate\Support\Facades\Facade::setFacadeApplication($this->container);

        $aliases = require __DIR__ . '/../app/Config/Aliases.php';
        \App\Foundation\AliasLoader::getInstance($aliases)->register();

        $this->registerProviders();

        add_action('template_redirect', 'redirect_canonical');
        add_action('template_redirect', 'wp_redirect_admin_locations');
        add_action('template_redirect', [$this, 'setRouter'], 20);
    }

    protected function registerProviders()
    {
        /*
         * Service providers.
         */

        //$this->container->register('\App\ServiceProviders\RouteServiceProvider');
        //$this->container->register('\App\Providers\Route');
        //$this->container->register('\App\Providers\PostType');

        $providers = require __DIR__ . '/../app/Config/Providers.php';
        foreach ($providers as $provider) {
            $this->container->register($provider);
        }
    }

    public function setRouter()
    {

        if (is_feed() || is_comment_feed()) {
            return;
        }

        try {
            $request = $this->container['request'];
            $response = $this->container['router']->dispatch($request);

            // We only send back the content because, headers are already defined
            // by WordPress internals.
            $response->sendContent();
        } catch (\Symfony\Component\HttpKernel\Exception\NotFoundHttpException $exception) {
            /*
             * Fallback to WordPress templates.
             */
        }
    }

    public static function instance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new static();
        }

        return static::$instance;
    }
}

