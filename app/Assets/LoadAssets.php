<?php

namespace App\Assets;

class LoadAssets
{

    public function __construct()
    {
        $this->register();
    }

    public function register()
    {
        add_action('wp_enqueue_scripts', [$this, 'parseAssets']);
        add_filter('script_loader_src', [$this, 'remove_script_version'], 15, 1);
        add_filter('style_loader_src', [$this, 'remove_script_version'], 15, 1);
    }

    public function parseAssets()
    {
        global $post;
        $slug = $post->post_name;

        $assetsJson = get_template_directory() . '/resources/assets/assets.json';

        if (is_readable($assetsJson)) {
            $assets = json_decode(file_get_contents($assetsJson), true);

            wp_enqueue_script(
                'bundle-js',
                get_stylesheet_directory_uri() . "/{$assets['bundle']['js']}",
                $deps = array(),
                '',
                true
            );
            wp_register_style(
                'bundle-css',
                get_stylesheet_directory_uri() . "/{$assets['bundle']['css']}"
            );
            wp_enqueue_style('bundle-css');

            if (array_key_exists('page/'.$slug, $assets)) {
                wp_enqueue_script(
                    'page-'. $slug,
                    get_stylesheet_directory_uri() . "/{$assets['page/'.$slug]['js']}",
                    $deps = array('bundle-js'),
                    '',
                    true
                );
                self::localizeScript('page-'. $slug);
            }

            $this->localizeScript('bundle-js');

        }

    }


    protected function localizeScript($handle)
    {

        // An array that may be populated by script variables for output with `wp_localize_script` after the footer script is enqueued
        $vars = array(
            'ajax_object' => array('ajax_url' => admin_url('admin-ajax.php'))
        );

        // Pass variables to scripts at runtime; must be triggered after the script is enqueued; see: http://codex.wordpress.org/Function_Reference/wp_localize_script
        if (!empty($vars)) {
            foreach ($vars as $var => $data) {
                wp_localize_script(
                    $handle,
                    $var,
                    $data
                );
            }
        }
    }
    public function remove_script_version($src) {
        return $src ? esc_url(remove_query_arg('ver', $src)) : false;
    }

}