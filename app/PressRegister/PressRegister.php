<?php

namespace App\PressRegister;

class PressRegister
{
    public $data;

    public function themeSupport(array $data)
    {
        foreach ($data as $feature => $value) {
            add_theme_support($feature, $value);
        }
    }

    public function menus(array $data)
    {
        foreach ($data as $slug => $desc) {
            $locations[$slug] = $desc;
        }
        register_nav_menus($locations);
    }

    public function classes($data)
    {
        $loader = new \Composer\Autoload\ClassLoader();
        foreach ($data as $prefix => $path) {
            $loader->addPsr4($prefix, $path);
        }
        $loader->register();
    }

    public function adminMenu(array $data)
    {

        $this->adminMenu = $data;
        add_action('admin_menu', [$this, 'removeMenus']);
    }

    public function removeMenus()
    {
        $this->conditions = [
            'Dashboard' => 'index.php',
            'Jetpack' => 'jetpack',
            'Posts' => 'edit.php',
            'Media' => 'upload.php',
            'Pages' => 'edit.php?post_type=page',
            'Comments' => 'edit-comments.php',
            'Appearance' => 'themes.php',
            'Plugins' => 'plugins.php',
            'Users' => 'users.php',
            'Tools' => 'tools.php',
            'Settings' => 'options-general.php',

        ];

        foreach ($this->conditions as $key => $value) {
            if (!in_array($key, $this->adminMenu)) {
                remove_menu_page($value);
            }
        }
    }

}