<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class PostType extends Facade
{
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */

    protected static function getFacadeAccessor()
    {
        return 'posttype'; // the IoC binding.
    }
}
