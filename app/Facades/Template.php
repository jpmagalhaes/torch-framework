<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Template extends Facade
{
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */

    protected static function getFacadeAccessor()
    {
        return 'Template'; // the IoC binding.
    }
}
