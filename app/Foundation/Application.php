<?php

namespace App\Foundation;

use Illuminate\Container\Container;
use Illuminate\Support\ServiceProvider;

class Application extends Container
{
    protected $loadedProviders = [];

    public function __construct()
    {
        $this->registerApplication();
    }

    /**
     * Register the Application class into the container,
     * so we can access it from the container itself.
     */
    public function registerApplication()
    {
        // Normally, only one instance is shared into the container.
        static::setInstance($this);
        $this->instance('app', $this);
    }

    public function register($provider, array $options = [], $force = false)
    {
        if (!$provider instanceof ServiceProvider) {
            $provider = new $provider($this);
        }
        if (array_key_exists($providerName = get_class($provider), $this->loadedProviders)) {
            return;
        }
        $this->loadedProviders[$providerName] = true;
        $provider->register();

        if (method_exists($provider, 'boot')) {
            $provider->boot();
        }
    }
}
