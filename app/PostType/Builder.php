<?php

namespace App\PostType;

class Builder
{
    protected $datas;

    protected $container;

    public function __construct ()
    {
        add_action('init', [$this, 'register'], 0);
    }

    public function make($name, $plural, $singular)
    {
        $params = compact('name', 'plural', 'singular');

        foreach ($params as $key => $param) {
            if (!is_string($param)) {
                throw new Exception('Invalid custom post type parameter "'.$key.'". Accepts string only.');
            }
        }

        // Set main properties.
        $this->datas['name'] = $name;
        $this->datas['args'] = $this->setDefaultArguments($plural, $singular);

        return $this;
    }

    public function set(array $params = [])
    {
        // Override custom post type arguments if given.
        $this->datas['args'] = array_replace_recursive($this->datas['args'], $params);

        $this->register();

        return $this;
    }


    protected function setDefaultArguments($plural, $singular)
    {
        $labels = [
            'name' => $plural,
            'singular_name' => $singular,
            'menu_name' => $plural
        ];

        $defaults = [
            'label' => $plural,
            'labels' => $labels,
            'description' => '',
            'public' => true,
            'menu_position' => 20,
            'has_archive' => true
        ];

        return $defaults;

    }

    public function register()
    {

        $this->postType = register_post_type($this->datas['name'], $this->datas['args']);

        // Register the status.
        if (!empty($this->status)) {
            foreach ($this->status as $key => $args) {
                register_post_status($key, $args);
            }
        }
    }
}