<?php

return [
    'App\Providers\RouteServiceProvider',
    'App\Providers\PostTypeServiceProvider',
    'App\Providers\TaxonomyServiceProvider',
    'App\Providers\HeaderCleanupServiceProvider',
    'App\Providers\LoadAssetsServiceProvider',
    'App\Providers\TemplateServiceProvider',
    'App\Providers\PressRegisterServiceProvider',
    'App\Providers\AjaxServiceProvider',
];