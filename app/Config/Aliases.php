<?php

return [
    'PostType' => 'App\Facades\PostType',
    'Route' => 'App\Facades\Route',
    'Taxonomy' => 'App\Facades\Taxonomy',
    'HeaderCleanup' => 'App\Facades\HeaderCleanup',
    'LoadAssets' => 'App\Facades\LoadAssets',
    'Template' => 'App\Facades\Template',
    'PressRegister' => 'App\Facades\PressRegister',
    'Ajax' => 'App\Facades\Ajax',
];