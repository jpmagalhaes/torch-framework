<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Ajax\Builder;

class AjaxServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('ajax', function ($container)
        {

            return new Builder();
        });
    }
}
