<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\PressRegister\PressRegister;

class PressRegisterServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('PressRegister', function ($container) {

            return new PressRegister();
        });
    }
}
