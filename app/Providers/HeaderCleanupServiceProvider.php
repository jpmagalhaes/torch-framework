<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\HeaderCleanup\HeaderCleanup;

class HeadercleanupServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('HeaderCleanup', function ($container) {

            return new HeaderCleanup();
        });
    }
}
