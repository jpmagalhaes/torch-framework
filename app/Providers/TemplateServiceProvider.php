<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Template\Builder;

class TemplateServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('Template', function ($container) {

            return new Builder();
        });
    }
}
