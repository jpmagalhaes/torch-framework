<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Taxonomy\Builder;

class TaxonomyServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('customTaxonomy', function ($container) {

            return new Builder();
        });
    }
}
