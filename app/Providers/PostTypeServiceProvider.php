<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\PostType\Builder;

class PostTypeServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('posttype', function ($container) {

            return new Builder();
        });
    }
}
