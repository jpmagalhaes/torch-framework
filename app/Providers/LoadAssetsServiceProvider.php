<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Assets\LoadAssets;

class LoadAssetsServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('LoadAssets', function ($container) {

            return new LoadAssets();
        });
    }
}
