<?php

namespace App\Route\Matching;

use Illuminate\Http\Request;
use App\Route\Route;

interface IMatching
{
    /**
     * Validate a given rule against a route and request.
     *
     * @param \App\Route\Route          $route
     * @param \Illuminate\Http\Request  $request
     *
     * @return bool
     */
    public function matches(Route $route, Request $request);
}
