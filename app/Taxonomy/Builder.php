<?php

namespace App\Taxonomy;

class Builder
{
    protected $datas;

    protected $container;

    protected $postTypes = [];

    public function make($name, $postTypes, $plural, $singular)
    {
        $params = compact('name', 'postType', 'plural', 'singular');

        foreach ($params as $key => $param) {
            if (!is_string($param)) {
                throw new Exception('Invalid custom post type parameter "'.$key.'". Accepts string only.');
            }
        }

        // Set main properties.
        $this->datas['name'] = $name;
        $this->datas['postTypes'] = $postTypes;
        $this->datas['args'] = $this->setDefaultArguments($plural, $singular);

        return $this;
    }

    public function set(array $params = [])
    {
        // Override custom post type arguments if given.
        $this->datas['args'] = array_replace_recursive($this->datas['args'], $params);

        $this->register();

        //return $this;
    }


    protected function setDefaultArguments($plural, $singular)
    {
        $labels = [
            'name' => $plural,
            'singular_name' => $singular,
            'menu_name' => $plural,

        ];

        $defaults = [
            'label' => $plural,
            'labels' => $labels,
            'description' => '',
            'public' => true,
            'hierarchical' => true,
            'show_ui' => true,
            'show_admin_column' => false,
            'show_in_nav_menus' => true,
            'show_tagcloud' => false,
            'single_value' => false

        ];

        return $defaults;
    }

    public function register()
    {
        $this->postType = register_taxonomy($this->datas['name'], $this->datas['postTypes'], $this->datas['args']);

    }
}