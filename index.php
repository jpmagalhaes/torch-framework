<?php
/*
Plugin Name: Torch Framework
Description: A wordpress framework
Version: 0.0.1
Author: João Magalhães <joao@orgo.ee>
*/

require_once __DIR__.'/bootstrap/app.php';

Torch::instance();